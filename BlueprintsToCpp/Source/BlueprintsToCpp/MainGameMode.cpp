// Fill out your copyright notice in the Description page of Project Settings.


#include "MainGameMode.h"
#include "Quests/QuestManager.h"
#include "GameFramework/Actor.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GamePlayStatics.h"

AMainGameMode::AMainGameMode()
{
	QuestManager = nullptr;
}

void AMainGameMode::BeginPlay()
{
	Super::BeginPlay();

	if (MainDisplayWidgetClass)
	{
		APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		MainDisplayWidget = CreateWidget(PlayerController, MainDisplayWidgetClass);

		if (MainDisplayWidget)
		{
			MainDisplayWidget->AddToViewport();
		}
	}
}

AQuestManager* AMainGameMode::GetQuestManager()
{
	if (!QuestManager)
	{
		QuestManager = GetWorld()->SpawnActor<AQuestManager>(QuestManagerBPClass);
	}

	return QuestManager;
}
