// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "QuestInfo.h"
#include "QuestManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCompletedQuestSignature, int32, Index);

UCLASS()
class BLUEPRINTSTOCPP_API AQuestManager : public AInfo
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AQuestManager();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void CompleteQuest(FName QuestId, bool CompleteWholeQuest);

	UFUNCTION(BlueprintPure)
	FQuestInfo GetQuest(FName Name) const;

	UFUNCTION(BlueprintCallable)
	bool IsActiveQuest(FName QuestId) const;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FCompletedQuestSignature CompletedQuest;

	UFUNCTION(BlueprintCallable)
	TArray<FQuestInfo> GetQuests() const;

	UFUNCTION(BlueprintCallable)
	bool IsQuestComplete(FQuestInfo QuestInfo) const;

	UFUNCTION(BlueprintCallable)
	bool IsActiveIndex(int32 Index) const;

	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (DefaultToSelf = "WorldContextObject", HidePin = "WorldContextObject"))
	static AQuestManager* GetQuestManager(const UObject* WorldContextObject);

protected:
	UFUNCTION(BlueprintCallable)
	int32 GetQuestIndex(FName QuestId) const;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FQuestInfo> QuestList;

};
