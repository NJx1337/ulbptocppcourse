// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestManager.h"
#include "../MainGameMode.h"

// Sets default values
AQuestManager::AQuestManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

int32 AQuestManager::GetQuestIndex(FName QuestId) const
{
	for (int ArrayIndex = 0; ArrayIndex < QuestList.Num(); ArrayIndex++)
	{
		if (QuestList[ArrayIndex].QuestId == QuestId)
		{
			return ArrayIndex;
		}
	}
	return 0;
}

void AQuestManager::CompleteQuest_Implementation(FName QuestId, bool CompleteWholeQuest)
{
	int32 QuestIndex = GetQuestIndex(QuestId);
	FQuestInfo Quest = QuestList[QuestIndex];
	if (CompleteWholeQuest)
	{
		QuestList[QuestIndex].Progress = Quest.ProgressTotal;
	}
	else
	{
		QuestList[QuestIndex].Progress = FMath::Min(Quest.Progress + 1, Quest.ProgressTotal);
	}
	CompletedQuest.Broadcast(QuestIndex);
}

FQuestInfo AQuestManager::GetQuest(FName Name) const
{
	return QuestList[GetQuestIndex(Name)];
}

bool AQuestManager::IsActiveQuest(FName QuestId) const
{
	int32 QuestIndex = GetQuestIndex(QuestId);
	return IsActiveIndex(QuestIndex);
}

TArray<FQuestInfo> AQuestManager::GetQuests() const
{
	TArray<FQuestInfo> CopyArray = QuestList;
	return QuestList;
}

bool AQuestManager::IsQuestComplete(FQuestInfo QuestInfo) const
{
	return QuestInfo.Progress == QuestInfo.ProgressTotal;
}

bool AQuestManager::IsActiveIndex(int32 Index) const
{
	for (int ArrayIndex = 0 ; ArrayIndex < QuestList.Num(); ArrayIndex++)
	{
		if (ArrayIndex == Index)
		{
			return true;
		}
		else
		{
			if (!IsQuestComplete(QuestList[ArrayIndex]))
			{
				return false;
			}
		}
	}
	return false;
}

AQuestManager* AQuestManager::GetQuestManager(const UObject* WorldContextObject)
{
	AMainGameMode* GameMode = Cast<AMainGameMode>(WorldContextObject->GetWorld()->GetAuthGameMode());
	if (GameMode)
	{
		return GameMode->GetQuestManager();
	}
	return nullptr;
}
