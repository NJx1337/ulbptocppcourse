// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestTriggerPickup.h"
#include "../QuestManager.h"

// Sets default values for this component's properties
UQuestTriggerPickup::UQuestTriggerPickup()
{
	QuestId = "None";
}

void UQuestTriggerPickup::Pickup()
{
	AQuestManager::GetQuestManager(this)->CompleteQuest(QuestId, false);
}

// Called when the game starts
void UQuestTriggerPickup::BeginPlay()
{
	Super::BeginPlay();
}
