// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestTriggerVolume.h"
#include "../QuestManager.h"
#include "Components/BoxComponent.h"

// Sets default values
AQuestTriggerVolume::AQuestTriggerVolume()
{
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	SetRootComponent(Root);

	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	Box->SetupAttachment(RootComponent);

	QuestId = "None";
	bCompleteWholeQuest = false;

	bOverlapEventExecuted = false;
}

// Called when the game starts or when spawned
void AQuestTriggerVolume::BeginPlay()
{
	Super::BeginPlay();

	Box->OnComponentBeginOverlap.AddDynamic(this, &AQuestTriggerVolume::OnBeginOverlap);
}

void AQuestTriggerVolume::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (bOverlapEventExecuted) return;

	APawn* PawnActor = Cast<APawn>(OtherActor);

	if (PawnActor)
	{
		bOverlapEventExecuted = true;

		AQuestManager::GetQuestManager(this)->CompleteQuest(QuestId, bCompleteWholeQuest);
	}
}
