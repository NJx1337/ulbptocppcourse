// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "QuestTriggerCollect.generated.h"

UCLASS()
class BLUEPRINTSTOCPP_API AQuestTriggerCollect : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AQuestTriggerCollect();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void MarkItemSeen(AActor* NewItem);

	UFUNCTION(BlueprintCallable)
	bool IsCorrectItem(AActor* Item) const;
	
	UFUNCTION(BlueprintCallable)
	bool HasNotSeenItem(AActor* ItemToFind) const;

	UFUNCTION(BlueprintCallable)
	void NotifyQuestComplete();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USceneComponent* Root;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USphereComponent* Sphere;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName QuestId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName ItemTag;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSet<AActor*> SeenItems;

public:
	UFUNCTION()
	virtual void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};
