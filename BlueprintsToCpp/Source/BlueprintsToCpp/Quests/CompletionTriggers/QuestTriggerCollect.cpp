// Fill out your copyright notice in the Description page of Project Settings.


#include "QuestTriggerCollect.h"
#include "Components/SphereComponent.h"
#include "../QuestManager.h"

// Sets default values
AQuestTriggerCollect::AQuestTriggerCollect()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("SceneRoot"));
	SetRootComponent(Root);

	Sphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	Sphere->SetupAttachment(Root);
}

// Called when the game starts or when spawned
void AQuestTriggerCollect::BeginPlay()
{
	Super::BeginPlay();
	
	Sphere->OnComponentBeginOverlap.AddDynamic(this, &AQuestTriggerCollect::OnBeginOverlap);
}

void AQuestTriggerCollect::MarkItemSeen(AActor* NewItem)
{
	SeenItems.Emplace(NewItem);
}

bool AQuestTriggerCollect::IsCorrectItem(AActor* Item) const
{
	return Item->ActorHasTag(ItemTag);
}

bool AQuestTriggerCollect::HasNotSeenItem(AActor* ItemToFind) const
{
	bool bHasSeen = SeenItems.Contains(ItemToFind);
	return !bHasSeen;
}

void AQuestTriggerCollect::NotifyQuestComplete()
{
	AQuestManager::GetQuestManager(this)->CompleteQuest(QuestId, false);
}

void AQuestTriggerCollect::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (IsCorrectItem(OtherActor) && HasNotSeenItem(OtherActor))
	{
		MarkItemSeen(OtherActor);
		NotifyQuestComplete();
	}
}
