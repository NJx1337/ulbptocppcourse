// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MainGameMode.generated.h"

/**
 * 
 */
UCLASS()
class BLUEPRINTSTOCPP_API AMainGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMainGameMode();

	UFUNCTION(BlueprintCallable)
	class AQuestManager* GetQuestManager();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<AQuestManager> QuestManagerBPClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	AQuestManager* QuestManager;

private:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UUserWidget> MainDisplayWidgetClass;

	UPROPERTY();
	UUserWidget* MainDisplayWidget;
	
};
