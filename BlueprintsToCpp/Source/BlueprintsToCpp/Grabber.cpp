// Fill out your copyright notice in the Description page of Project Settings.


#include "Grabber.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Components/ActorComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Engine/EngineTypes.h"
#include "Quests/CompletionTriggers/QuestTriggerPickup.h"

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

void UGrabber::Grab()
{
	AActor* HitActor;
	UPrimitiveComponent* HitComponent;
	if (TraceForPhysicsBodies(HitActor, HitComponent))
	{
		HitComponent->SetSimulatePhysics(true);
		GetPhysicsComponent()->GrabComponentAtLocationWithRotation(HitComponent, NAME_None, HitComponent->GetCenterOfMass(), FRotator());
		NotifyQuestActor(HitActor);
	}
}

void UGrabber::Release()
{
	GetPhysicsComponent()->ReleaseComponent();
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	GetPhysicsComponent()->SetTargetLocationAndRotation(GetHoldLocation(), GetComponentRotation());
}

FVector UGrabber::GetMaxGrabLocation() const
{
	return GetComponentLocation() + GetComponentRotation().Vector() * MaxGrabDistance;
}

FVector UGrabber::GetHoldLocation() const
{
	return GetComponentLocation() + GetComponentRotation().Vector() * HoldDistance;
}

UPhysicsHandleComponent* UGrabber::GetPhysicsComponent() const
{
	return GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
}

void UGrabber::NotifyQuestActor(AActor* Actor)
{
	UQuestTriggerPickup* QuestTriggerPickup = Actor->FindComponentByClass<UQuestTriggerPickup>();

	if (QuestTriggerPickup)
	{
		QuestTriggerPickup->Pickup();
	}
}

bool UGrabber::TraceForPhysicsBodies(AActor*& HitActor, UPrimitiveComponent*& HitComponent)
{
	const FVector Start = GetComponentLocation();
	const FVector End = GetMaxGrabLocation();

	// https://answers.unrealengine.com/questions/252257/gethitresultundercursorforobjects-array.html
	TArray<TEnumAsByte<EObjectTypeQuery> > ObjectTypes;
	ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_PhysicsBody));

	FHitResult OutHit;

	bool Hit = UKismetSystemLibrary::SphereTraceSingleForObjects(GetWorld(), Start, End, GrabRadius, ObjectTypes, false, TArray<AActor*>(), EDrawDebugTrace::Type::None, OutHit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);

	if (Hit)
	{
		HitActor = OutHit.GetActor();
		HitComponent = OutHit.GetComponent();
	}

	return Hit;
}

